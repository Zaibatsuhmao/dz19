#include <iostream>
class Animal
{
    public:
    virtual void Sound() const = 0;
};

class Dog : public Animal
{
    public:
        void Sound() const override
        {
            std::cout << "Woof\n";
        }
};

class Cat : public Animal
{
    public:
        void Sound() const override
        {
            std::cout << "Meow\n";
        }
};

class Cow : public Animal
{
public:
    void Sound() const override
    {
        std::cout << "Muu\n";
    }
};

int main()
{
    Animal* animals[3];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Cow();

    for (Animal* a : animals)
        a->Sound();
}